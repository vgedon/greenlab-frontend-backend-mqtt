var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/mqtt';

/* GET users listing. */
router.get('/', function(req, res, next) {
	var test = [];
	// Use connect method to connect to the server
	MongoClient.connect(url, function(err, db) {
	  assert.equal(null, err);
	  console.log("Connected successfully to server");



	  var collection = db.collection('myCollections');
	  // Find some documents
	  collection.find({'topic': 'sensors'}).toArray(function(err, docs) {
	    assert.equal(null, err);
	    console.log("Found the following records");
	    // console.log(docs);
		for (var i = 0; i < docs.length; i++) {
			// console.log(parseInt(docs[i].value.toString()));
			test.push({'id': docs[i].options.clientId,'value' : parseInt(docs[i].value.toString())});
		}
		// console.log(typeof docs);
		console.log(docs.length);
	    // callback(docs);
		// test.copy(docs);
		console.log(test);
		db.close();
	  });
	});
  res.send('data is coming');
});

module.exports = router;
