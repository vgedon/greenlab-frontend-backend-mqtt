#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define SLEEP_DELAY_IN_SECONDS  30

const char* ssid     = "LAB42";
const char* password = "W1f1@L@b42!";

const char* mqtt_server = "10.42.43.146";
const char* mqtt_username = "esp8622";
const char* mqtt_password = "";
const char* mqtt_topic = "sensors";

WiFiClient espClient;
PubSubClient client(espClient);

int sensorPin = A0; //Reading pin
int sensorPower = 0; //sensor power to turn on when needed only
int lastReading = 0; //through testing the sensor values ranges from 777 no contact to 100 in tap water
char moistureString[5];

void setup() {
  // setup serial port
  Serial.begin(115200);
  //setup pins
  pinMode(sensorPin,INPUT); //setting the pin modes

  // setup WiFi
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client", mqtt_username, mqtt_password)) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//Function to read the sensor value
int readSensor() {
  //first we need to power on the sensor so we set the sensorPower On
  digitalWrite(sensorPower,HIGH);
  //wait half a second for the sensor to stabilize
  delay(500);
  Serial.println("digitalRead sensorPower");
  Serial.println(digitalRead(sensorPower));
  //read take 100 readings to make sure the sensor is working correctly
  for (int i=0;i<100;i++){
    analogRead(sensorPin);
  }
  //take a reading as store it
  lastReading = analogRead(sensorPin);
  Serial.println("lastReading");
  Serial.println(lastReading);
  String(lastReading).toCharArray(moistureString, 5);
  //turn off the sensor to avoid corrosion :)
  digitalWrite(sensorPower,LOW);
}

void loop() {
  setup_wifi();
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  // read moisture value
  readSensor();
  Serial.print("Sending moisture value: ");
  Serial.println(lastReading);

  // send moisture value to the MQTT topic
  client.publish(mqtt_topic, moistureString);

  Serial.println("Closing MQTT connection...");
  client.disconnect();

  Serial.println("Closing WiFi connection...");
  WiFi.disconnect();

  delay(100);

  //Serial << "Entering deep sleep mode for " << SLEEP_DELAY_IN_SECONDS << " seconds..." << endl;
  //ESP.deepSleep(SLEEP_DELAY_IN_SECONDS * 1000000, WAKE_RF_DEFAULT);
  //ESP.deepSleep(10 * 1000, WAKE_NO_RFCAL);
  delay(5000);   // wait for deep sleep to happen
}
